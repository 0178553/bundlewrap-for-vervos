files = {
    '/etc/identifier': {
        'content': "${node.hostname}",
        'owner': "root",
        'group': "root",
        'mode': '0775',
    },
    '/root/.bashrc': {
        'source': "bashrc",
        "content_type": "text",
        'owner': "root",
        'group': "root",
    },
    '/etc/vim/vimrc': {
        'source': "vimrc",
        'content_type': "text",
        'owner': 'root',
        'group': 'root',
        'needs': ['pkg_apt:',],
    },
    '/etc/vim/vimrc.local': {
        'source': "vimrc.local",
        'content_type': "text",
        'owner': 'root',
        'group': 'root',
        'needs': ['pkg_apt:',],
    },
    '/home/mreinhardt/.ssh/authorized_keys': {
        'source': "authorized_keys",
        'content_type': "text",
        'owner': "mreinhardt",
        "group": "mreinhardt",
    },
    '/etc/init/gunicorn.conf': {
        'source': "gunicorn.conf",
        'content_type': 'text',
        'owner': 'root',
        'group': 'root',
    },
    '/etc/init/celery.conf': {
        'source': "celery.conf",
        'content_type': 'text',
        'owner': 'root',
        'group': 'root',
    },
    '/etc/nginx/sites-available/vervos.net': {
        'source': "vhost-vervos",
        'content_type': 'text',
        'owner': 'root',
        'group': 'root',
        'triggers': ['action:restart_nginx',],
    },
    '/etc/crontab': {
        'source': "crontab",
        'content_type': 'text',
        'owner': 'root',
        'group': 'root',
    },

}

actions = {
    'restart_nginx': {
        'command': "service nginx restart && service nginx reload",
        'triggered': True,
    },
}

pkg_pip = {
    'gunicorn': {},
}

symlinks = {
    '/etc/nginx/sites-enabled/vervos.net': {
        'group': "root",
        'owner': "root",
        'target': "/etc/nginx/sites-available/vervos.net",
    },
}

users = {
    'mreinhardt':  {
        'home': "/home/mreinhardt",
        'shell': "/bin/bash",
    },
}

pkg_apt = {
    'git': {},
    'htop': {},
    'libpq-dev': {},
    'nginx': {},
    'postgresql': {},
    'postgresql-contrib': {},
    'python3.4': {},
    'python3-dev': {},
    'python-virtualenv': {},
    'vim': {},
}
